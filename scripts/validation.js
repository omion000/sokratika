import {GetEl,Err} from "./classList-v_1.0.js";
import { controller as fieldOne  } from "./field-space.js";


class SearchParametr
{
    constructor()
    {
        this.searchAttr  = '';
        this.attrName    = [];
        this.statusField = {};
        this.formSubmitButton = '';
    }
}
class SearchParametrBuilder
{
    constructor()
    {
        this.searchParametr = new SearchParametr();
    }
    setSearchAttr(searchAttr )
    {
        this.searchParametr.searchAttr = searchAttr;
        return this;
    }
    setAttrName(attrName)
    {
        this.searchParametr.attrName = attrName;
        return this;
    }
    setStatusField(statusField)
    {
        this.searchParametr.statusField = statusField;
        return this;
    }
    setFormSubmitButtonAttribute(formSubmitButton)
    {
        this.searchParametr.formSubmitButton = formSubmitButton;
        return this;
    }
}

class FormValidator extends SearchParametrBuilder
{
    constructor(form)
    {
        super();
        this.form = form;
        this.arrInput = [];
        this.statusField = new Map();
        this.submitButton;
        this.resArr = [];
    }
    fieldCollection()
    {
        const data = this.searchParametr.searchAttr;
        const attrName = this.searchParametr.attrName;
        const form = this.form;
        attrName.forEach((el)=>{
            const serchStr = `[${data}="${el}"]`;
            const input = form.querySelector(serchStr);
            this.arrInput.push(input);
        });
        return this;
    }
    validationOnline()
    {
        const inputArr = this.arrInput;

        validationInputArr(inputArr);

        inputArr.forEach((el) =>
        {
            const attrName = this.removeAfterDash(el.dataset.inputType);
            let valinpt = '';
            el.addEventListener('input', (elem) =>
            {
                const targetVal = elem.target.value;
                let targetValLength = "";
                let valLength = 0;

                let regExpForType;
                const contextTel = this;
                switch (attrName)
                {
                    case 'name':
                        regExpForType = /[\d:;\?<>*\/\!\"\'\+\{\}\[\],№.=\\@#$%^&\(\)\|\-_~`]/gi;
                        inputData(el,regExpForType);
                        break;
                    case 'message':
                        regExpForType = /[;<>*\/\'\+\{\}\[\]<>=\\#$%^&\|~`]/gi;
                        inputData(el,regExpForType);
                        break;
                    case 'tel':
                        regExpForType = /[A-Za-zА-Яа-яЁё :;\?<>*\/\!\"\'\+\-\{\}\[\],№.=\\(@)#$%^&\(\)\|_~`]/gi;
                        targetValLength = targetVal.slice(0,11);
                        valLength = 11;
                        break;
                    case 'eMail':
                        regExpForType = /[А-Яа-яёЁ :;\?<>*\/\!\"\'\+\{\}\[\],№.=\#$%^&\(\)\|\~`]/gi;
                        inputData(el,regExpForType);
                        break;
                    case 'tel_eMail':
                        regExpForType = /[А-Яа-яёЁ :;\?<>*\/\!\"\'\+\{\}\[\],№=\#$%^&\(\)\|\~`]/gi;
                        inputData(el,regExpForType);
                        break;
                    case 'file':
                        break;
                    case 'checkbox':
                        break;

                    default:
                        throw {
                            code: 406,
                            message: `Обработчик для ${attrName} неопределён`
                        }
                        break;
                }
                if( targetVal.length > valLength && valLength != 0 )
                {
                    inputData(el,targetVal, targetValLength);
                }else
                {
                    inputData(el,regExpForType);
                }
            });
        });
        return this;

        function inputData(el,regexp,val = "")
        {
            let elValue = el.value;
            el.value = elValue.replace( regexp, val );
        }

        function validationInputArr(inputArr)
        {
            if(inputArr.includes(null))
            {
                inputArr.forEach((el)=>
                {
                    if(el)
                    {
                        el.dataset.input = 'disabled';
                    }
                });
                fieldOne.inputDisabled();
                new Err().err('418','' +
                    'В переменной inputArr, метода validationOnline. ' +
                    'Один из элементов массива null, должен входить массив инпутов' +
                    '');
            }
        }
    }

    validationSetStatusSend()
    {
        this.validationSend();
        this.setStatusInput();
        this.sendForm();

        return this;
    }
    validationSend()
    {
        const submitbutton = this.submitButton;
        const inputArr = this.arrInput;
        const status = this.searchParametr.statusField;
        let regExpForType;

        submitbutton.addEventListener('click',(event) =>
        {
            event.preventDefault();
            inputArr.forEach((el) =>
            {
                const attrName = el.dataset.inputType;
                const attrName1 = this.removeAfterDash(el.dataset.inputType);

                switch (attrName1)
                {
                    case 'name':
                        regExpForType = /^[\w\W]{3,25}$/i;
                        break;
                    case 'message':
                        regExpForType =/^[\w\W]{5,200}$/mi;
                        break;
                    case 'tel':
                        regExpForType = /^[7|8]{1}[\d]{10}$/i;
                        break;
                    case 'eMail':
                        regExpForType = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
                        break;
                    case 'tel_eMail': 
                        regExpForType = /^[7|8]{1}[\d]{10}$/i;
                        let res = this.inputStatus( el,regExpForType );

                        regExpForType = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
                        res = !res?!this.inputStatus( el,regExpForType )?0:1:1;
                        this.resArr.push(res);
                        this.statusField.set( attrName, res );

                        break;
                    case 'file':
                        this.resArr.push(1);
                        break;
                    case 'checkbox':
                        if (el.checked) {
                            this.resArr.push(1);
                            this.statusField.set( attrName, 1 );
                        } else {
                            this.resArr.push(0);
                            this.statusField.set( attrName, 0 );
                        }
                        break;

                    default:
                        throw {
                            code: 406,
                            message: `field not declareted, not handler for type ${attrName}`
                        }
                        break;
                }

                if( attrName !== 'file' && attrName !== 'tel_eMail' && attrName !== 'checkbox')
                {
                    const res = this.inputStatus( el,regExpForType );
                    this.resArr.push(res);
                    this.statusField.set( attrName, res );
                }
            });
        });
        return this;
    }

    setStatusInput()
    {
        const submitbutton = this.submitButton;

        submitbutton.addEventListener('click',() =>
        {
            this.statusField.forEach((val,key)=>{

                const data = this.searchParametr.searchAttr;
                const typeStatusArr = this.searchParametr.statusField;
                const serchStr = `[${data}="${key}"]`;

                const typeStatus = val?typeStatusArr.success:typeStatusArr.error;
                const valParametr = typeStatus.split('=');
                valParametr[1] = valParametr[1].toString().replaceAll('"','');

                const form = this.form;
                const input = form.querySelector(serchStr);
                input.closest('.field-space').setAttribute(valParametr[0],valParametr[1]);

                return this;
            });
        });
    }

    inputStatus(el,regexp) 
    {
        let elValue = el.value;
        if( elValue.match( regexp ) ){
            return 1;
        }
        return 0;
    }

    setSubmitButton()
    {
        this.submitButton = this.form.querySelector( this.searchParametr.formSubmitButton );
        return this;
    }

    sendForm()
    {
        const submitbutton = this.submitButton;


        submitbutton.addEventListener('click',() =>
        {
            const captchaState = this.captchaValidate();
            this.resArr.push(captchaState);

            console.log(this.resArr);

            setTimeout(()=>
            {   
                if(!this.resArr.includes(0)){
                    alert('Форма была бы отправлена')
                }else{
                    alert('Форма не была бы отправлена')
                }
                this.resArr = [];
            },500)

        });
    }

    sendFormGrecapcha3()
    {
        const submitbutton = this.submitButton;

        submitbutton.addEventListener('click',() =>
        {
            grecaptcha.ready(() => {
                grecaptcha.execute('6LcnquokAAAAACTXgxa4rvwFmP1kMeJ42Ykm-ypJ', {action: 'submit'}).then((token) => {
                    if(!this.resArr.includes(0) && token.length > 0)
                    {
                        const path = './i.php';
                        const form = new FormData(this.form);
                        this.xhr('POST',path, form)
                            .then( (response) =>
                            {
                                let container = this.form.querySelector('[data-attr="successfully-sent"]');
                                container.innerHTML = "Форма успешно отправлена!";
                            })
                            .catch(function (err)
                            {
                                alert('Ошибка ответа сервера, свяжитесь с администратором')
                            });
                    }else
                    {
                    }
                    this.resArr = [];
                });
            });
        });
        function getInputValues(inputs) {
            let values = "";
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].name) {
                    values += `${inputs[i].name}: ${inputs[i].value} `;
                }
            }
            return values;
        }
    }

    captchaValidate()
    {
        const captcha = document.querySelector('[data-attr="captcha"]'); 
        const textCaptchaErr = `<div data-attr="captcha-warning"><span class="captcha-err text-smal">Капча не была пройдена</span></div>`;
        let sendButton = document.querySelector('[data-form="send"]');

        const captchaResponse = "1231212312312"
        const captchaLength = captchaResponse.length;

        if(!captchaLength)
        {
            captcha.insertAdjacentHTML('afterend',textCaptchaErr);
            return 0;
        }else
        {
            const warningBlock = document.querySelector('[data-attr="captcha-warning"]');
            if( warningBlock ) warningBlock.remove();
            return 1;
        }
        grecaptcha.reset()
    }

    xhr(method = '',path= '', body = null)
    {
        return new Promise(function (resolve, reject)
        {
            const xhr = new XMLHttpRequest();
            xhr.open(method, path );

            xhr.onprogress = function ()
            {
                console.log(xhr.readyState);
            }
            xhr.onload = function ()
            {
                console.log(xhr.status) 
                console.log(xhr.readyState);
                console.log(xhr.response) 
                resolve(xhr.response)
            }
            xhr.onerror = function ()
            {
                reject(xhr.response)
            }
            xhr.send(body);
        });
    }

    removeAfterDash(str='')
    {
        if (str.includes('-')) {
            const parts = str.split('-');
            return parts[0];
        } else {
            return str;
        }
    }
}

export {FormValidator}
