class ButtonAction
{
    constructor(elForAnimation,dashOffSet)
    {
        this.interval;
        this.elForAnimation = elForAnimation;
        this.dashOffset     = dashOffSet;
        this.dashOffsetBase = dashOffSet;
    }
    setPropAnimation(typeAction,el)
    {
        el.querySelector('path').classList.add('transition');
        if(typeAction == 'mouseenter' || typeAction == 'mouseleave'){
            el.addEventListener(typeAction,(event)=>
            {
                const target = event.target;
                const path   = target.querySelector('path[data-attr="dinamic-path"]');
                const speed  = 15;
                const step   = 7;
                if( typeAction == 'mouseenter')
                {
                    this.settingAnimation(typeAction,path,speed,step);
                }
                if( typeAction == 'mouseleave')
                {
                    this.settingAnimation(typeAction,path,speed,step);
                }
            });
        }else{
            throw 'ERROR invalid TypeAction. Only "mouseenter" & "mouseout"';
        }
        return 0;
    }
    settingAnimation(typeAction,path,speed,step)
    {
        clearInterval(this.interval);
        this.interval = setInterval(()=>
        {
            if(typeAction == 'mouseenter')
            {
                this.dashOffset -=step;
                if(this.dashOffset <= 0){
                    clearInterval(this.interval);
                    this.dashOffset = 0
                }
            }
            else
            {
                this.dashOffset +=step;
                if(this.dashOffset >= this.dashOffsetBase){
                    clearInterval(this.interval);
                    this.dashOffset = this.dashOffsetBase;
                }
            }
            path.style.strokeDashoffset = this.dashOffset;
        },speed);
    }
    callPropAnimationAll(el = this.elForAnimation)
    {
        this.setPropAnimation('mouseenter',el);
        this.setPropAnimation('mouseleave',el);
    }
}

class CreateEl
{
    constructor(nameNode,parentNodeName)
    {
        this._nameNode       = nameNode;
        this._parentNodeName = parentNodeName;
    }

    get nameNode()
    {
        return this._nameNode;
    }

    get parentNodeName()
    {
        return this._parentNodeName;
    }

    create(elClassListArr = new Array(), attrListObj )
    {
        const el = document.createElement(`${this.nameNode}`);
        elClassListArr.forEach(function (e)
        {
           el.classList.add(`${e}`);
        });
        const parent = this.parentNodeName;
        parent.prepend(el);
        return el;
    }

}

class ValidationForm
{
    constructor(form,type)
    {
        this._form = form;
        this._type = type;
    }
    get form ()
    {
        return this._form;
    }
    get type ()
    {
        return this._type;
    }
    getInputField()
    {
        const inputListArr = [];
        const formQuestions = this._form;
        [].forEach.call(formQuestions, function (e)
        {
            const nodeNameE = (e.nodeName).toLowerCase();
            if(nodeNameE === 'input')
            {
                inputListArr.push(e)
            }
        });
        return inputListArr;
    }

    validationInput()
    {
        const inputList = this.getInputField();
        let   babel     = this;
        inputList.forEach(function (eInput) {
            const lenArr = [0,0];
            eInput.addEventListener('input',function ()
            {
                const typeName = this.dataset.inputValidatorType;
                let regExp;
                const contextTel = this;
                switch (typeName)
                {
                    case 'name':
                        regExp = /[\d:;\?<>*\/\!\"\'\+\{\}\[\],№.=\\@#$%^&\(\)\|\-_~`]/gi;
                        inputData(contextTel,regExp);
                        console.log(typeName, 'name');
                        break;
                    case 'message': 
                        regExp = /[;<>*\/\'\+\{\}\[\],.=\\#$%^&\(\)\|~`]/gi;
                        inputData(this,regExp);
                        console.log(typeName, 'message');
                        break;
                    case 'tel':
                        const thisVal = contextTel.value;
                        const thisLen = thisVal.length;
                        lenArr[0] = thisLen;
                        lenArr[1] = lenArr[0] == lenArr[1]?lenArr[0]-1:lenArr[1];
                        if(lenArr[0] > lenArr[1])
                        {
                            contextTel.value = thisLen <= 1?`+7${thisVal}`:thisLen > 12?thisVal.slice(0,thisLen-1):thisVal;
                            const thisVal_1 = contextTel.value;
                            const thisLen_1 = thisVal_1.length;

                            regExp = /\D/gi;
                            let lastElValue = thisVal_1.slice(-1);
                            let elValue = thisVal_1.slice(0,thisLen_1-1);
                            contextTel.value = elValue + lastElValue.replace( regExp, "" );


                        }
                        lenArr[1] = thisLen;
                        break;
                    case 'eMail': 
                        regExp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
                        inputData(this,regExp);
                        console.log(typeName);
                        break;
                    default:
                        throw {
                            code: 406,
                            message: "field not declareted"
                        }
                        break;

                }
            })
        });

        function inputData(el,regexp){
            let elValue = el.value;
            el.value = elValue.replace( regexp, "" );
        }

        function keysShortcutDisabled(input, ...codes)
        {
            let pressed = new Set();
            document.addEventListener('keydown', function(event)
            {
                pressed.add(event.code);
                for (let code of codes)
                {
                    if (!pressed.has(code))
                    {
                        return;
                    }
                }
                pressed.clear();
                input.value +=' ';
                input.value = input.value.slice(0,-1);
                console.log(input)
            });

            document.addEventListener('keyup', function(event)
            {
                pressed.delete(event.code);
            });
        }
    }

    validationSend()
    {
        const inputList = this.getInputField();

    }
}

class GetEl
{
    constructor(el,resultKey = 1)
    {
        this.el = el;
        this.resultKey = resultKey;
    }
    get()
    {
        let elList = document.querySelectorAll(this.el);
        return elList.length == 1 && this.resultKey?elList[0]:elList;
    }
}

class Err
{
    constructor() {}

    static err(errNumber, errMsg)
    {
        throw {
            code: errNumber,
            message: errMsg
        }
    }
}

export { ButtonAction, ValidationForm, CreateEl, GetEl, Err }