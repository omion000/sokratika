import {GetEl} from './classList-v_1.0.js'
import {FormValidator} from './validation.js'

document.addEventListener('DOMContentLoaded',function()
{
    controller.transmitter();
});

const controller =
{
    transmitter: function () {
        this.template();
        this.heightVh();
        this.customSelect();
        this.quickSelectionEnterInput();
        this.quickSelectionOpen();
        this.countWords();
        this.inputValidation();
        this.fieldClear();
        this.fieldsetfocus();
        this.btnActive();

    },
    template: function()
    {
    },
    getEl: function(el,resultKey = 1)
    {
        let elList = document.querySelectorAll(el);
        return elList.length == 1 && resultKey?elList[0]:elList;

    },
    heightVh: function()
    {
        (function init100vh()
        {
            function setHeight()
            {
                var vh = window.innerHeight * 0.01;
                document.documentElement.style.setProperty('--vh', `${vh}px`);
            }
            setHeight();
            window.addEventListener('resize', setHeight);
        })();
    },
    buttonAction: function()
    {
        const buttonList = this.getEl('.button',0);
        buttonList.forEach((e)=>{
            const el = e.closest('.form')
            if(e.closest('.form'))
            {
                const btnAction = new ButtonAction(e,600);
                btnAction.callPropAnimationAll();
            }
            else
            {
                const btnAction = new ButtonAction(e,416);
                btnAction.callPropAnimationAll();
            }
        });
    },
    customSelect() {
        const customSelects = document.querySelectorAll('[data-custom-select]');

        customSelects.forEach(select =>
        {
            const options = select.querySelectorAll('option');
            const customSelectDiv = document.createElement('div');
            const customSelectClass = select.getAttribute('data-custom-select');

            customSelectDiv.classList.add('custom-select');
            customSelectDiv.classList.add(customSelectClass);

            const selectBox = document.createElement('div');
            selectBox.classList.add('select-box');
            customSelectDiv.appendChild(selectBox);

            const selectText = document.createElement('div');
            selectText.textContent = options[0].textContent;
            selectBox.appendChild(selectText);
            selectBox.dataset.fieldsetInput="";

            const arrow = document.createElement('div');
            arrow.innerHTML =
                '<svg width="24" height="24" viewBox="0 0 24 24" >\n' +
                '    <use xlink:href="#arrow-down"></use>\n' +
                '</svg>'; 
            selectBox.appendChild(arrow);

            const optionsList = document.createElement('div');
            optionsList.classList.add('options');
            optionsList.dataset.fieldsetInput="";

            options.forEach(option => {
                const optionDiv = document.createElement('div');
                optionDiv.textContent = option.textContent;
                optionDiv.dataset.value = option.value;

                optionDiv.addEventListener('click', () => {
                    const value = optionDiv.dataset.value;
                    select.value = value;
                    selectText.textContent = optionDiv.innerText;
                    optionsList.classList.remove('open');
                    arrow.classList.remove('arrow-up');
                    this.setState(selectBox,0)

                    customSelectDiv.querySelectorAll('.options div').forEach(item => {
                        item.classList.remove('active');
                    });

                    optionDiv.classList.add('active');

                    const existingLabel = optionDiv.querySelector('.options__selected-label');
                    if (!existingLabel) {
                        const selectedLabel = document.createElement('span');
                        selectedLabel.classList.add('options__selected-label');
                        selectedLabel.innerHTML = '' +
                            '<svg width="24" viewBox="0 0 24 24">\n' +
                            '    <use xlink:href="#check-mark"></use>\n' +
                            '</svg>';
                        optionDiv.appendChild(selectedLabel);
                    }
                });

                optionsList.appendChild(optionDiv);
            });

            selectBox.addEventListener('click', () =>
            {
                optionsList.classList.toggle('open');
                arrow.classList.toggle('arrow-up');
                this.setState(selectBox,1)
            });

            document.addEventListener('click', (e) =>
            {
                if (!customSelectDiv.contains(e.target))
                {
                    optionsList.classList.remove('open');
                    arrow.classList.remove('arrow-up');
                    this.setState(selectBox,0)
                }

            });

            select.classList.add('hidden'); 
            customSelectDiv.appendChild(optionsList);
            select.parentNode.insertBefore(customSelectDiv, select);
        });
    },
    quickSelectionEnterInput()
    {
        const quickSelection = document.querySelector('[data-quick-selection]');
        const quickSelectionInput = document.querySelector('[data-quick-selection-input]');

        if (quickSelection && quickSelectionInput) {
            quickSelection.addEventListener('click', function(event) {
                if (event.target.matches('span')) {
                    const selectedText = event.target.textContent;
                    quickSelectionInput.value = selectedText;
                }
            });
        }
    },
    quickSelectionOpen()
    {
        this.toggle({
            nameAttrInitClick: 'data-quick-selection-button',
            nameAttrMutable: [
                'data-quick-selection'
            ]
        },0,1)
    },
    toggle(
        nameAttr= {
            nameAttrInitClick: '',
            nameAttrMutable: []
        },defaultState='0',endState='1'
    )
    {
        const nameAttrInitClick = nameAttr.nameAttrInitClick;
        const elClick = document.querySelector(`[${nameAttrInitClick}]`);

        elClick.addEventListener('click',function ()
        {
            for (let i = 0; i < nameAttr.nameAttrMutable.length; i++)
            {
                try {
                    const nameAttrTrigger = nameAttr.nameAttrMutable[i]
                    const elMutable = document.querySelector(`[${nameAttrTrigger}]`);
                    changeState(elMutable,nameAttrTrigger);
                }catch (e){
                    throw 'str:14, вероятно querySelector == null';
                }
            }
        })
        function changeState(elTrigger,mutableAttribute)
        {
            const dataSetFormat = dataAttrFormatForJs(mutableAttribute);
            let dataState = elTrigger.dataset[dataSetFormat];
            dataState == defaultState?
                elTrigger.dataset[dataSetFormat] = endState:
                elTrigger.dataset[dataSetFormat] = defaultState;
        }

        function dataAttrFormatForJs(nameAttr)
        {
            let strFilter = nameAttr.replaceAll('data-','');
            let state = 0, strArr = [];
            for (let strFilterKey in strFilter)
            {
                let symbol = strFilter[strFilterKey];
                symbol = state > 0?symbol.toUpperCase():symbol;
                state = symbol == '-'?1:0;
                strArr.push(symbol);
            }
            let str = strArr.join('');
            const dataAttrFormatForJs = str.replaceAll('-','');
            return dataAttrFormatForJs;
        }
    },
    countWords()
    {
        const textArea = document.querySelector('[data-count-words-textarea]');
        const currentCount = document.querySelector('[data-count-words-current]');
        const maxCount = document.querySelector('[data-count-words-max]');
        const countWord = document.querySelector('[data-cwords]');
        const maxTextLength = 200;

        if (textArea && currentCount && maxCount)
        {
            textArea.addEventListener('input', ()=>
            {
                const text = textArea.value;
                const currentTextLength = text.length;

                currentCount.textContent = currentTextLength;

                if (currentTextLength > maxTextLength) {
                    this.setState(textArea,0,'validation','label');
                    this.setState(textArea,2);
                    this.setState(countWord,2,'cwords','span');

                }else{
                    this.setState(textArea,1);
                    this.setState(textArea,1,'validation','label');
                    this.setState(countWord,1,'cwords','span');

                }

                maxCount.textContent = maxTextLength;
            });
        }

    },
    inputValidation()
    {   
        const form = new GetEl(`[data-attr="form-questions"]`).get();
        new FormValidator(form)
            .setSearchAttr("data-input-type")
            .setAttrName(['message','message-1'])
            .setStatusField({'success':`data-validation=1`, 'error':`data-validation=0`})
            .setFormSubmitButtonAttribute('[data-form="send"]')
            .setSubmitButton()
            .fieldCollection()
            .validationOnline()
            .validationSetStatusSend();
    },
    fieldClear(){
        const clearButton = document.querySelector('[data-field-clear]');
        const inputToClear = document.querySelector('[data-field-clear-input]');

        function clearInput()
        {
            inputToClear.value = ''; 
        }

        clearButton.addEventListener('click', clearInput);
    },
    fieldsetfocus()
    {
        const inputElement = document.querySelectorAll('[data-fieldset-input]');

        const elEvent = (el,event='', state) =>
        {
            return el.addEventListener(event, ()=>
            {
                this.setState(el, state);
            });
        }

        inputElement.forEach( (el)=>
        {
            elEvent(el,'focus',1);
            elEvent(el,'blur',0);
        });


    },
    setState(elem,state = 0,findAttr = 'fieldset',tagname='fieldset')
    {
        const el = elem.closest(`[data-${findAttr}]`);
        const elName = el.tagName.toLowerCase();
        if(elName == tagname)el.setAttribute(`data-${findAttr}`,state);
    },
    btnActive()
    {
        const inputs = document.querySelectorAll('[data-input-type]');
        const button = document.querySelector('[data-btn-active]');

        button.setAttribute('disabled', 'disabled');

        inputs.forEach(input => {
            input.addEventListener('input', () => {
                const totalCharacters = Array.from(inputs)
                    .map(input => input.value.length)
                    .reduce((acc, curr) => acc + curr, 0);

                const allInputsCount = Array.from(inputs).every(input => input.value.length > 4);

                if (totalCharacters > 8 && allInputsCount) {
                    button.removeAttribute('disabled');
                } else {
                    button.setAttribute('disabled', 'disabled');
                }
            });
        });


    }

}


