import { CreateEl, Err } from "./classList-v_1.0.js";



const controller =
{
    transmitter() {
        this.inputLabelUp();
        this.inputDisabled();
        this.multipleCustomFileType();
    },

    inputLabelUp()
    {
        const fieldOneInput = document.querySelectorAll('[data-label]');
        fieldOneInput.forEach(function (el)
        {
            const statusLabel = el.dataset.label;
            if ( statusLabel == '1' )
            {
                const input = el.querySelector('input');
                input.addEventListener('input', function ()
                {
                    const valLength = this.value.length;
                    this.dataset.labelUp = valLength ? '1' : '0'
                });
                return 0;
            } else if ( statusLabel == '0' )return 0;
            Err.err('400',"Синтаксическая ошибка inputLabelUp");
        });
    },

    inputDisabled()
    {

        const inputField =  this.getEl('input[data-input="disabled"]',0);
        [].forEach.call(inputField, function (el)
        {
            el.disabled = true;
        });
    },

    multipleCustomFileType: function()
    {
        const babel = this;
        const inputFile = babel.getEl('[data-file-field]', 0);
        const containerPast = babel.getEl('[data-attr="file-content-past"]');

        [].forEach.call(inputFile, function (elInputFile, index)
        {
            const createEl = new CreateEl('div',containerPast[index]);

            elInputFile.addEventListener("change", function (event)
            {
                const el = event.target;
                clearContainer(elInputFile, containerPast[index]);

                (function(el)
                {
                    const filesList = el.files;

                    [].forEach.call(filesList,function (e)
                    {
                        const fileName = e.name;
                        const typeFileExample = ['doc', 'docx', 'pdf', 'jpg', 'png' ];
                        for (const val of typeFileExample) {
                            const regExp = new RegExp(`.${val}$`);
                            const result = regExp.test(`${fileName}`);
                            let classList = ['file__type', 'file__view-elements'];

                            const img = ( val === 'png' || val === 'jpg' ) && result;
                            const doc = ( val === 'doc' || val === 'docx' ) && result;
                            const pdf = ( val === 'pdf' ) && result;

                            if( pdf )
                            {
                                classList.push('_pdf');
                                createEl.create(classList);
                            }
                            else if( doc )
                            {
                                classList.push('_doc');
                                createEl.create(classList);
                            }
                            else if( img )
                            {
                                classList.push('_img');
                                let contImg = createEl.create(classList);
                                babel.blob(e,contImg);
                            }
                        }
                    });
                })(el);
            });
        });
        return 0;


        function clearContainer(inputFile, containerPast)
        {
            let listChildrenContainerPast = containerPast.children;
            let listChildrenLength = listChildrenContainerPast.length;
            for (let i = listChildrenContainerPast.length - 1; i >= 0 ; i--) {
                const classFileAdd = listChildrenContainerPast[i].classList.contains('file__add');
                if( !classFileAdd )
                {
                    listChildrenContainerPast[i].remove();
                }
            }
        }
    },

    getEl: function(el, resultType = 1)
    {
        let elList = document.querySelectorAll(el);
        return elList.length == 1 && resultType?elList[0]:elList;
    },

    blob: function(file,targetImgContayner)
    {
        const blob = new Blob([file],{type: 'image/jpeg'})
        targetImgContayner.style.backgroundImage = `url(${URL.createObjectURL(blob)})`;
        targetImgContayner.style.backgroundSize = 'contain'
    }
}

export { controller }